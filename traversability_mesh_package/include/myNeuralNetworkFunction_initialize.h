//
//  Academic License - for use in teaching, academic research, and meeting
//  course requirements at degree granting institutions only.  Not for
//  government, commercial, or other organizational use.
//
//  myNeuralNetworkFunction_initialize.h
//
//  Code generation for function 'myNeuralNetworkFunction_initialize'
//


#ifndef MYNEURALNETWORKFUNCTION_INITIALIZE_H
#define MYNEURALNETWORKFUNCTION_INITIALIZE_H

// Include files
#include <cstddef>
#include <cstdlib>
#include "rtwtypes.h"
#include "myNeuralNetworkFunction_types.h"

// Function Declarations
extern void myNeuralNetworkFunction_initialize();

#endif

// End of code generation (myNeuralNetworkFunction_initialize.h)
