# CMake generated Testfile for 
# Source directory: /root/Desktop/Thesis_workspace/src/Traversability_module/Robot/lcm-1.4.0/test
# Build directory: /root/Desktop/Thesis_workspace/src/Traversability_module/Robot/lcm-1.4.0/build/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("types")
subdirs("c")
subdirs("cpp")
subdirs("python")
